<?php
function sendConfirmationMail( $name, $email )
{
    if( empty( $name ) || empty( $email ) )
    {
        return false;
    }

    $subject = 'AgroAs - Askaban i Artemis F1 - potwierdzenie wypełnienia formularza';

    ob_start();
?>
<!DOCTYPE html>
<html lang="pl">
    <head>
        <title><?= $subject; ?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
    </head>
    <body>
        <img src="https://promocja.agroas.pl/img/agroas-logo.png" src="AgroAs">
        <p style="font-weight: bold">Dziękujemy za przesłanie formularza!<p>
        <p>Już wkrótce skontaktujemy się z&nbsp;Tobą, aby przekazać informacje o&nbsp;odmianach, którymi jesteś zainteresowany.</p>
        <p>Do usłyszenia<br>zespół AgroAs</p>
    </body>
</html>
<?php
    mail( "{$name} <{$email}>", $subject, ob_get_clean(), "MIME-Version: 1.0\r\nContent-type: text/html; charset=UTF-8\r\nFrom: AgroAs <auto@agroas.pl>\r\n" );
}

function sendConfirmationMailToAA( $name, $mail, $phone, $consent_1, $consent_2 )
{
    ob_start();
?>
<!DOCTYPE html>
<html lang="pl">
    <head>
        <title><?= ( $subject = 'Nowy kontakt – AgroAs - Askaban i Artemis F1' ); ?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
    </head>
    <body>
        <img src="https://promocja.agroas.pl/img/agroas-logo.png" src="AgroAs">
        <p style="font-weight: bold">Nowy lead w&nbsp;LP Askaban i&nbsp;Artemis F1</p>
        <p>Nowa osoba wysłała swoje zgłoszenie poprzez formularz:<p>
        <p>Imię i&nbsp;nazwisko: <strong><?= $name ?? '–'; ?></strong><br>E-mail: <strong><?= $mail ?? '–'; ?></strong><br>Telefon: <strong><?= $phone ?? '–'; ?></strong><br>Zgoda nr 1: <strong><?= empty( $consent_1 ) ? 'Nie' : 'Tak'; ?></strong><br>Zgoda nr 2: <strong><?= empty( $consent_2 ) ? 'Nie' : 'Tak'; ?></strong></p>
        <p>Do usłyszenia<br>zespół AgroAs</p>
    </body>
</html>
<?php
    mail( 'AgroAs <zamowienia@agroas.pl>', $subject, ob_get_clean(), "MIME-Version: 1.0\r\nContent-type: text/html; charset=UTF-8\r\nFrom: AgroAs <auto@agroas.pl>" );
}

?>