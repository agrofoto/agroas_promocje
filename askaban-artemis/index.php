<?php
require("functions.php");
redirect();
?>
<!DOCTYPE HTML>
<html lang="pl">
<head>
    <title>Pszenica Askaban i rzepak Artemis F1 – AgroAs</title>
    <meta charset="utf-8">
    <meta name="description" content="">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <link rel="preconnect" href="https://fonts.googleapis.com">
    <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
    <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@400;700&display=swap" rel="stylesheet">
    <link rel="stylesheet" type="text/css" href="./css/grid.css">
    <link rel="stylesheet" type="text/css" href="./css/style.css?v=9">
    <link rel="icon" href="./img/favicon.png" type="image/png">

    <!-- Google Tag Manager -->
    <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
    new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
    j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
    'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
    })(window,document,'script','dataLayer','GTM-MG7C3M6');</script>
    <!-- End Google Tag Manager -->

</head>
<body>

    <!-- Google Tag Manager (noscript) -->
    <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-MG7C3M6"
    height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <!-- End Google Tag Manager (noscript) -->

<?php

thankYouPage();

if ( !isset( $_GET['thank-you'] ) ) :

?>

<header class="relative">

    <div class="row relative">
        <div class="l6 xxs8 col">
            <img src="img/agroas-logo.png" srcset="img/agroas-logo.png 1x, img/agroas-logo@2x.png 2x" alt="AgroAs - logo" id="logo">
        </div>

        <div class="clearfix"></div>

        <div class="l6 m12 col">
            <div class="pre-title">Najlepsza oferta na</div>
            <h1>pszenicę Askaban<br>i&nbsp;rzepak Artemis&nbsp;F1</h1>
            <a href="#formularz" class="pre-title small">
                <p><strong>Wypełnij formularz,<br>zamów w&nbsp;najlepszej cenie</strong>i&nbsp;odbierz w&nbsp;prezencie miarkę (5&nbsp;l) do&nbsp;dawkowania nawozów i&nbsp;ŚOR.</p>
                <div class="miarka">
                    <p>Gratis!</p>
                    <img src="img/miarka.png?v=2" srcset="img/miarka.png?v=2 1x, img/miarka@2x.png?v=2 2x" alt="Miarka 5 l">
                </div>
            </a>
        </div>

        <div class="clearfix"></div>

        <div class="big-buttons">
            <div class="l5 s6 xs12 col">
                <a href="#pszenica-askaban" class="big-button left-margin">
                    <img src="img/icon-pszenica.svg" alt="">
                    Szukasz odmiany <strong>pszenicy ozimej</strong> z&nbsp;ponadprzeciętną zdrowotnością?
                </a>
            </div>
            <div class="l5 s6 xs12 col right">
                <a href="#rzepak-artemis-f1" class="big-button">
                    <img src="img/icon-rzepak.svg" alt="">
                    Potrzebna Ci odmiana <strong>rzepaku ozimego</strong> o&nbsp;bardzo wysokim potencjale?
                </a>
            </div>
            <div class="clearfix"></div>
        </div>

    <?php
 
    if ( !isset( $_GET['thank-you'] ) ) :

        //---------------
        //-- FORMULARZ --
        //---------------

        $validation = false;
        $error = array();

        saveData();

        if ( $validation == false || count( $error ) > 0 ) {

            form( 1 );

        }

    endif;

    ?>

    </div>

    <picture>
        <source media="(max-width: 660px)" srcset="img/background-mobile.jpg">
        <img src="img/background.jpg" srcset="img/background.jpg 1x, img/background@2x.jpg 2x" alt="" class="background">
    </picture>

</header>

    <section class="row phones">

        <div class="l12 text-center">

            <p><strong>Zadzwoń do nas i spytaj naszych ekspertów o szczegóły:</strong><br>
            <a href="tel:+48660405387">+48 660 405 387</a>
            </p>

        </div>

    </section>

    <section>

        <div class="row" id="pszenica-askaban">

            <div class="l5 s12 col l-push-7 m-dont-push">
                <h2>Pszenica Askaban</h2>
                <span class="subtitle">Czarodziej plonów</span>
                <p class="big">Pszenica Askaban to idealny wybór. Jest unikalnym połączeniem wysokiego plonu oraz wysokiej zimotrwałości.</p>
                <p class="small">Szukasz nasion na siew pszenicy ozimej, które zapewnią wysoki plon oraz wysoką zimotrwałość?</p>
            </div>

            <div class="l7 s12 col l-pull-5 m-dont-pull relative text-center">
                <img src="img/icon-pszenica.svg" alt="" class="badge">
                <img src="img/pszenica-askaban.jpg" srcset="img/pszenica-askaban.jpg 1x, img/pszenica-askaban@2x.jpg 2x" alt="Pszenica Askaban" class="circle">
            </div>

        </div>

        <div class="row">

            <div class="l12 grey icons">
                <div class="l4 xs12 col">
                    <img src="img/icon-pszenica-wigor.svg" alt="">
                    <p>Wśród jej najważniejszych cech jest <strong>doskonały wigor jesienny i&nbsp;wiosenny</strong>.</p>
                </div>
                <div class="l4 xs12 col">
                    <img src="img/icon-pszenica-siew.svg" alt="">
                    <p>Askaban nadaje się do <strong>opóźnionych terminów siewu</strong>.</p>
                </div>
                <div class="l4 xs12 col">
                    <img src="img/icon-pszenica-zdrowotnosc.svg" alt="">
                    <p>Charakteryzuje się <strong>ponadprzeciętną zdrowotnością</strong> na&nbsp;wszystkie najważniejsze choroby.</p>
                </div>
            </div>

        </div>

        <div class="row">
            <div class="l12 features">
                <div class="l1 s-hide">
                    &nbsp;
                </div>
                <div class="l6 s6 xs12 col">
                    <span>Cechy rolnicze:</span>
                    <ul>
                        <li>Termin kłoszenia: <strong>średniowczesny</strong>,</li>
                        <li>Termin dojrzewania: <strong>średniowczesny</strong>,</li>
                        <li>MTZ: <strong>wysoki</strong>,</li>
                        <li>Odporność na wyleganie: <strong>wysoka do bardzo wysokiej</strong>,</li>
                        <li>Wysokość rośliny: <strong>średnia</strong>,</li>
                        <li>Zimotrwałość: <strong>4,5</strong></li>
                        <li>Liczba opadania: <strong>wysoka</strong>.</li>
                    </ul>
                </div>
                <div class="l5 s6 xs12 col">
                    <span>Odporność na choroby:</span>
                    <ul>
                        <li>Mączniak prawdziwy: <strong>wysoka</strong>,</li>
                        <li>Fuzarioza kłosów: <strong>wysoka</strong>,</li>
                        <li>Rdza brunatna: <strong>wysoka</strong>,</li>
                        <li>Rdza żółta: <strong>bardzo wysoka</strong>,</li>
                        <li>Septorioza plew: <strong>średnia do wysokiej</strong>,</li>
                        <li>Septorioza liści: <strong>średnia do wysokiej</strong>.</li>
                    </ul>
                </div>
                <div class="l1 s-hide">
                    &nbsp;
                </div>
            </div>
        </div>
        <div class="row">
            <div class="l12 text-center">
                <a href="#formularz" class="button">Zostaw kontakt</a>
            </div>
        </div>

    </section>


    <!-- Dodatkow odmiany -->

    <section id="nowe-pszenice">

        <div class="row">
            <p class="big text-center">
                Dostępne również
            </p>
        </div>

        <div class="row">

            <div class="l6 s12 col" id="pszenica-patras">

                <div class="frame">

                    <div class="l12 col">

                        <img src="img/icon-pszenica.svg" alt="" class="badge">
                        <h2 class="green">Pszenica Patras</h2>
                        <p><strong>Charakterystyka:</strong> typ pojedynczo-kłosowy z dobrą stabilnością plonu ziarna i szybkim napełnieniem ziarna, szybki rozwój początkowy, dobra zdrowotność liści, dobra tolerancja na fuzariozę, rośliny niskie, łatwy omłot dzięki równomiernej dojrzałości roślin.</p>

                    </div>

                    <div class="features">

                        <div class="l6 xs12 col">
                            <span>Cechy rolnicze:</span>
                            <ul>
                                <li>Termin kłoszenia: <strong>średniowczesny</strong>,</li>
                                <li>Termin dojrzewania: <strong>średniowczesny</strong>,</li>
                                <li>MTZ: <strong>wysoki</strong>,</li>
                                <li>Odporność na wyleganie: <strong>średnia do wysokiej</strong>,</li>
                                <li>Wysokość rośliny: <strong>średnia</strong>,</li>
                                <li>Zimotrwałość: <strong>4</strong></li>
                                <li>Liczba opadania: <strong>wysoka</strong>.</li>
                            </ul>
                        </div>
                        <div class="l6 xs12 col">
                            <span>Odporność na choroby:</span>
                            <ul>
                                <li>Mączniak prawdziwy: <strong>dobra</strong>,</li>
                                <li>Fuzarioza kłosów: <strong>dobra</strong>,</li>
                                <li>Rdza brunatna: <strong>dobra</strong>,</li>
                                <li>Rdza żółta: <strong>wysoka</strong>,</li>
                                <li>Septorioza plew: <strong>średnia do dobrej</strong>,</li>
                                <li>Septorioza liści: <strong>dobra</strong>.</li>
                            </ul>
                        </div>
                    </div>

                    <div class="l12 text-center l-hide m-hide s-show">
                        <a href="#formularz" class="button">Zostaw kontakt</a>
                    </div>

                </div>

            </div>


            <div class="l6 s12 col" id="pszenica-turandot">

                <div class="frame">

                    <div class="l12 col">

                        <img src="img/icon-pszenica.svg" alt="" class="badge">
                        <h2 class="green">Pszenica Turandot</h2>
                        <p><strong>Charakterystyka:</strong> wysoka zdrowotność i jakość, przydatna również do uprawy w późniejszym terminie siewu po kukurydzy na ziarno, wysoki plon zabezpieczony wysokim MTN, dobra tolerancja na okresowe niedobory wody, podwyższona odporność na septoriozę plew oraz fuzariozę kłosów, duża odporność na wyleganie, znakomita gospodarka wodna, typ kompensacyjny wykazujący zdolność rekompensowania i nadrabiania pewnych niedociągnięć.</p>

                    </div>

                    <div class="features">

                        <div class="l6 xs12 col">
                            <span>Cechy rolnicze:</span>
                            <ul>
                                <li>Termin kłoszenia: <strong>średni</strong>,</li>
                                <li>Termin dojrzewania: <strong>średni</strong>,</li>
                                <li>MTZ: <strong>wysoki</strong>,</li>
                                <li>Odporność na wyleganie: <strong>średnia</strong>,</li>
                                <li>Wysokość rośliny: <strong>średnia</strong>,</li>
                                <li>Zimotrwałość: <strong>4</strong></li>
                                <li>Liczba opadania: <strong>wysoka</strong>.</li>
                            </ul>
                        </div>
                        <div class="l6 xs12 col">
                            <span>Odporność na choroby:</span>
                            <ul>
                                <li>Mączniak prawdziwy: <strong>wysoka</strong>,</li>
                                <li>Fuzarioza kłosów: <strong>wysoka</strong>,</li>
                                <li>Rdza brunatna: <strong>wysoka</strong>,</li>
                                <li>Rdza żółta: <strong>wysoka</strong>,</li>
                                <li>Septorioza plew: <strong>wysoka</strong>,</li>
                                <li>Septorioza liści: <strong>wysoka</strong>.</li>
                            </ul>
                        </div>
                    </div>

                    <div class="l12 text-center l-hide m-hide s-show">
                        <a href="#formularz" class="button">Zostaw kontakt</a>
                    </div>

                </div>

            </div>

            <div class="l12 text-center s-hide">
                <a href="#formularz" class="button">Zostaw kontakt</a>
            </div>

        </div>

    </section>

    <div class="row expanded no-padding grey">

        <section>

            <div class="row" id="rzepak-artemis-f1">
                <div class="l7 s12 col">
                    <h2>Rzepak Artemis F1</h2>
                    <span class="subtitle">Strzał w dziesiątkę</span>
                    <p class="big">Rzepak Artemis F1 da Ci&nbsp;120% wzorca. Najnowsza odmiana z&nbsp;odpornością</p>
                    <p class="small">Potrzebne Ci są nasiona rzepaku ozimego? Planujesz siew rzepaku ozimego?</p>
                </div>

                <div class="l5 s12 col relative text-center">
                    <img src="img/icon-rzepak.svg" alt="" class="badge">
                    <img src="img/rzepak-artemis.jpg" srcset="img/rzepak-artemis.jpg 1x, img/rzepak-artemis@2x.jpg 2x" alt="Rzepak Artemis" class="circle">
                </div>
            </div>

            <div class="row">
                <div class="l12 white icons">
                    <div class="l4 xs12 col">
                        <img src="img/icon-rzepak-odpornosc.svg" alt="">
                        <p>Artemis to najnowsza odmiana rzepaku z <strong>odpornością na wirusa żółtaczki rzepy</strong> (TuYV) Artemis posiada gen Rlm7, co&nbsp;daje rzepakowi odporność na&nbsp;suchą zgniliznę kapustnych.</p>
                    </div>
                    <div class="l4 xs12 col">
                        <img src="img/icon-rzepak-pekanie.svg" alt="">
                        <p>Jest też wysokoodporny na <strong>pękanie łuszczyn i osypywanie</strong> się nasion w&nbsp;niekorzystnych warunkach pogodowych.</p>
                    </div>
                    <div class="l4 xs12 col">
                        <img src="img/icon-rzepak-plonowanie.svg" alt="">
                        <p>I co ważne, jest to odmiana rzepaku o <strong>bardzo wysokim potencjale plonowania</strong> (średni plon 2017/2018 –&nbsp;120%&nbsp;wzorca).</p>
                    </div>
                </div>
            </div>

            <div class="row">
                <div class="l12 features">

                    <div class="l1 s-hide">
                        &nbsp;
                    </div>
                    <div class="l6 s6 xs12 col">
                        <span>Norma wysiewu:</span>
                        <ul>
                            <li>wczesny termin siewu: <strong>35–40 szt./m<sup>2</sup></strong></li>
                            <li>optymalny termin siewu: <strong>40–45 szt./m<sup>2</sup></strong></li>
                            <li>opóźniony termin siewu: <strong>45–50 szt./m<sup>2</sup></strong></li>
                        </ul>
                    </div>
                     <div class="l5 s6 xs12 col">
                        <span>Średni plon Artemis:</span>
                        <ul>
                            <li>2017/2018 – 120% wzorca – 49,3 dt/ha,</li>
                            <li>2019 – 128% wzorca – 45,1 dt/ha</li>
                        </ul>
                    </div>
                    <div class="l1 s-hide">
                        &nbsp;
                    </div>

                    <div class="l12 text-center">
                        <a href="#formularz" class="button">Zostaw kontakt</a>
                    </div>

                </div>
            </div>

        </section>

    </div>

<?php

endif;

?>

    <footer>

        <div class="row">
            <div class="l6 xs12 col">
                <h3>Kim jesteśmy?</h3>
            </div>
            <div class="l6 xs12 col">
                <img src="img/agroas-logo-mini.png" srcset="img/agroas-logo-mini.png 1x, img/agroas-logo-mini@2x.png 2x" alt="AgroAs - logo">
            </div>
        </div>
         <div class="row">
            <div class="l6 xs12 col">
                <p class="bold">AGROAS został założony w&nbsp;1992 roku i&nbsp;jest liderem rynku działającym w branży kompleksowego zaopatrzenia rolnictwa i&nbsp;obrotu płodami rolnymi.</p>
                <p>Obsługujemy ponad 5000 klientów – rolników indywidualnych oraz gospodarstwa wielkoobszarowe, spółdzielnie rolnicze i&nbsp;spółki działające w&nbsp;sektorze rolnym.</p>
                <p>Zadzwoń do nas i spytaj naszych ekspertów:<br>
                    <a href="tel:+48660405387">+48 660 405 387</a>
                </p>
            </div>
            <div class="l6 xs12 col">
                <p>Prowadzimy też szeroko zakrojone działania badawczo-rozwojowe nad udoskonalaniem procesu produkcji, zarówno zabiegów chemicznych, jak również samej techniki upraw gleby z&nbsp;wykorzystaniem najnowszych rozwiązań z&nbsp;zakresu nawożenia oraz ochrony roślin.</p>
                <p>Zdobyte doświadczenia wspomagają proces sprzedaży i&nbsp;przygotowania oferty dla naszych klientów a&nbsp;przede wszystkim pomagają nam lepiej zrozumieć ich potrzeby.</p>
            </div>
        </div>

        <div class="row footer-bottom">

            <div class="l6 s12 col">
                <p>© AgroAs 2021. Wszystkie prawa zastrzeżone.</p>
            </div>
            <div class="l6 s12 col">
                <ul>
                    <li><a href="https://agroas.pl/polityka-cookies" target="_blank">Polityka cookies</a></li>
                    <li><a href="http://www.sklep.agroas.pl/polityka-prywatnosci,18" target="_blank">Polityka prywatności</a></li>
                    <li><a href="/agroas-regulamin.pdf" target="_blank">Regulamin</a></li>
                </ul>
            </div>

        </div>

    </footer>

</div>

<script src="./js/cookie.js"></script>
<script src="./js/form.js?v=3"></script>

</body>
</html>
