function checkboxValidation( element ) {
    element.querySelector( 'input' ).addEventListener( 'change', function() {
        if ( element.querySelector( 'input' ).checked == true ) {
            element.querySelector( 'span' ).className = '';
        } else {
            element.querySelector( 'span' ).className = 'show';
        }
    })
}

function inputValidation( element ) {
    element.querySelector( 'input' ).addEventListener( 'keyup', function() {
        if ( element.querySelector( 'input' ).value != '' ) {

            if ( element.querySelector( 'input[name="phone"]' ) && validatePhone( element.querySelector('input[name="phone"]').value ) == false ) {
                element.querySelector( 'span' ).className = 'show';
                element.querySelector( 'input' ).className = 'error';
            } else {
                element.querySelector( 'span' ).className = '';
                element.querySelector( 'input' ).className = '';
            }
        } else {
            element.querySelector( 'span' ).className = 'show';
            element.querySelector( 'input' ).className = 'error';
        }
    })
}

function validateEmail( email ) {
    var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
    return re.test(String(email).toLowerCase());
}

function validatePhone( phone ) {
    phone = phone.replace(/\s/g, "");
    phone = phone.replace('-', "");
    var phoneno = /^([+]?\d{1,2}[.-\s]?)?(\d{3}[.-]?){2}\d{3}$/;
    return phoneno.test(String(phone).toLowerCase());
}

function checkInputsAndActiveSendButton() {

    var check = [];
    var inputsAll = document.querySelectorAll('.formularz input[type="text"], .formularz input[type="checkbox"][required]');
    var inputs = document.querySelectorAll('.formularz input[type="text"]');
    var checkboxes = document.querySelectorAll('.formularz input[type="checkbox"][required]');

    // Sprawdzenie wszystkich pól
    for ( var i = 0; i < inputsAll.length; i++ ) {
        check[ inputsAll[i].getAttribute('name') ] = false;
    }

    // Zdarzenia dla pól tekstowych
    for ( var i = 0; i < inputs.length; i++ ) {

        if ( inputs[i].value != '' ) {
            check[ inputs[i].getAttribute('name') ] = true;
        } else {
            check[ inputs[i].getAttribute('name') ] = false;
        }

        checkInputs( check );

        inputs[i].addEventListener( 'keyup', function( ) {

            for ( var i = 0; i < inputs.length; i++ ) {
                if ( inputs[i].value != '' ) {
                    check[ inputs[i].getAttribute('name') ] = true;
                } else {
                    check[ inputs[i].getAttribute('name') ] = false;
                }
            }

            checkInputs( check );

        });
    }

    // Zdarzenia dla checkboxów
    for ( var i = 0; i < checkboxes.length; i++ ) {

        if ( checkboxes[i].checked ) {
            check[ checkboxes[i].getAttribute('name') ] = true;
        } else {
            check[ checkboxes[i].getAttribute('name') ] = false;
        }

        checkInputs( check );

        checkboxes[i].addEventListener( 'change', function( ) {

            for ( var i = 0; i < checkboxes.length; i++ ) {

                if ( checkboxes[i].checked ) {
                    check[ checkboxes[i].getAttribute('name') ] = true;
                } else {
                    check[ checkboxes[i].getAttribute('name') ] = false;
                }
            }

            checkInputs( check );

        });
    }
}

function checkInputs( check ) {
    var activeSendButton = false;

    for ( var k in check ) {
        if ( check.hasOwnProperty(k) ) {
            if ( check[k] == true ) {
                activeSendButton = true;
            } else {
                activeSendButton = false;
                break;
            }
        }
    }

    if ( activeSendButton == true ) {
        document.querySelector('.formularz .button').setAttribute( 'disabled', 'false' );
        // document.querySelector('.formularz .form_submit').disabled = false;
    } else {
        document.querySelector('.formularz .button').setAttribute( 'disabled', 'true' );
        // document.querySelector('.formularz .form_submit').disabled = true;
    }
}

function checkForErrors() {

    var submit = document.querySelector(".form_submit");

    event.preventDefault();

    var error = false;
    var errors = [];

    if ( document.querySelector('.form input[name="name"]').value == '' ) {
        error = true;
        errors.push("name");
    }

    if ( document.querySelector('.form input[name="email"]').value != '' &&  validateEmail( document.querySelector('.form input[name="email"]').value ) == false  ) {
        error = true;
        errors.push("email");
    }

    if ( document.querySelector('.form input[name="phone"]').value == '' || ( document.querySelector('.form input[name="phone"]').value != '' && validatePhone( document.querySelector('.form input[name="phone"]').value ) == false ) ) {
        error = true;
        errors.push("phone");
    }

    if ( document.querySelector('.form input[name="zgoda_handlowa"]').checked == false ) {
        error = true;
        errors.push("zgoda_handlowa");
    }

    if ( document.querySelector('.form input[name="zgoda_handlowa2"]').checked == false ) {
        error = true;
        errors.push("zgoda_handlowa2");
    }

    if ( error == false ) {

        document.querySelector( '.form' ).submit();

    } else {

        for ( var i = 0; i<errors.length; i++ ) {

            if ( document.querySelector( '.form .field.' + errors[i] + ' input[type="checkbox"]' ) ) {
                var txt = 'Zaznaczenie tego pola jest obowiązkowe';
                checkboxValidation( document.querySelector( '.form .field.' + errors[i] ) );
            } else {
                var txt = 'Wypełnij poprawnie te pole';
                inputValidation( document.querySelector( '.form .field.' + errors[i] ) );
            }

            document.querySelector( '.form .field.' + errors[i] + ' span' ).innerHTML = txt;
            document.querySelector( '.form .field.' + errors[i] + ' span' ).className = 'show';
            document.querySelector( '.form .field.' + errors[i] + ' input' ).className = 'error';

        }

        document.querySelector( '.formularz' ).scrollIntoView({behavior: "smooth", block: "start", inline: "nearest"});

        return false;

    }

}

function alignModels() {

    var models = document.querySelectorAll('.frame');

    if ( window.innerWidth > 800 ) {

        for ( var i = 0; i < models.length; i++ ) {
            models[i].style.height = 'auto';
        }

        var max = 0;

        for ( var i = 0; i < models.length; i++ ) {

            if ( models[i].offsetHeight > max ) {
                max = models[i].offsetHeight;

            }

        }

        for ( var i = 0; i < models.length; i++ ) {
            models[i].style.height = max + 'px';
        }

    } else {

        for ( var i = 0; i < models.length; i++ ) {
            models[i].style.height = 'auto';
        }

    }

}

window.addEventListener('load', function() {

    alignModels();
    checkInputsAndActiveSendButton();

    if ( document.querySelector('.formularz .button') ) {
        document.querySelector('.formularz .button').addEventListener('click', function() {

            checkForErrors();

        });
    }

    if ( document.querySelector( '#zostaw-telefon' ) ) {
        document.querySelector( '#zostaw-telefon' ).addEventListener( 'click', function() {
            document.querySelector( '.formularz' ).scrollIntoView({behavior: "smooth", block: "start"});
            document.querySelector( '.formularz input[name="name"]').focus();
        });
    }

});

window.addEventListener('resize', function() {

    alignModels();

});

