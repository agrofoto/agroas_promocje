<?php
ob_start();

function redirect() {
    session_start();
    if ( isset($_GET['thank-you']) && $_SESSION['send'] != true ) {
        $_SESSION['send'] = false;
        header('location: https://promocja.agroas.pl/');
    }
    $_SESSION['send'] = false;
}

function thankYouPage() {

    if ( isset( $_GET['thank-you'] ) ) : ?>

        <div class="thank-you">

            <header class="row">
                <div class="l6 xxs8 col">
                    <img src="img/agroas-logo.png" srcset="img/agroas-logo.png 1x, img/agroas-logo@2x.png 2x" alt="AgroAs - logo" id="logo">
                </div>
            </header>

            <div class="grey">
                <section class="row">

                    <div class="l12 col text-center">

                        <img src="img/icon-sent.svg" alt="Wiadomość wysłana">
                        <p><strong>Dziękujemy za przesłanie formularza!</strong></p>
                        <p>Już wkrótce skontaktujemy się z Tobą, aby przekazać informacje o odmianach, którymi jesteś zainteresowany.</p>
                        <p>Do usłyszenia,<br>
                        zespół AgroAs!
                        </p>

                    </div>

                </section>
            </div>

        </div>

    <?php

    endif;

}

function saveData() {

    if ( $_SERVER['REQUEST_METHOD'] == 'POST') :

        $data = filter_input_array(INPUT_POST, array(
            "name" => FILTER_SANITIZE_STRING,
            "phone" => FILTER_SANITIZE_STRING,
            "email" => FILTER_SANITIZE_EMAIL
        ) );

        $data["zgoda_handlowa"] = ( isset( $_POST['zgoda_handlowa'] ) ) ? 1 : 0;
        $data["zgoda_handlowa2"] = ( isset( $_POST['zgoda_handlowa2'] ) ) ? 1 : 0;
        $date = date('Y-m-d H:i:s');

        if ( !empty( $_SERVER['HTTP_CLIENT_IP'] ) ) {
            $ip = $_SERVER['HTTP_CLIENT_IP'];
        } elseif ( !empty( $_SERVER['HTTP_X_FORWARDED_FOR'] ) ) {
            $ip = $_SERVER['HTTP_X_FORWARDED_FOR'];
        } else {
            $ip = $_SERVER['REMOTE_ADDR'];
        }

        $error = array();
        if ( trim($data['name']) == '' ) { $error["name"] = true; }
        if ( trim($data['phone']) == '' ) { $error["phone"] = true; }
        if ( $data['zgoda_handlowa'] != 1 ) { $error["zgoda_handlowa"] = true; }
        if ( $data['zgoda_handlowa2'] != 1 ) { $error["zgoda_handlowa2"] = true; }

        // Dodaj do bazy
        if ( count( $error ) == 0 ) {

            $mysqli = new mysqli( 'localhost', '00354226_adagri', 'adagri2021!', '00354226_adagri' );
            mysqli_set_charset($mysqli, "utf8");
            $mysqli->query("SET SESSION sql_mode=''");

            $check = $mysqli->query("SELECT * FROM lp_askaban_artemis WHERE phone = '".$data['phone']."' ");
            $checkrow = $check->num_rows;

            if ( $checkrow == null ) {

                $result = $mysqli->query("INSERT INTO lp_askaban_artemis VALUES ('', '".$data['name']."', '".$data['phone']."', '".$data['email']."', '".$data['zgoda_handlowa']."', '".$data['zgoda_handlowa2']."', '".$date."', '".$ip."' ) ") or $mysqli->error;

            } else {

                $result = true;

            }

            if ( $result ) {

                $_SESSION['send'] = true;

                require('mail.php');
                // Wyślij maila
                if ( !empty( $data['email'] ) ) {
                    sendConfirmationMail( $data['name'], $data['email'] );
                }

                sendConfirmationMailToAA( $data['name'], $data['email'], $data['phone'], $data['zgoda_handlowa'], $data['zgoda_handlowa2']);

                header ('location: ?thank-you');

            } else {

                echo '<p>Błąd, skontaktuj się z nami.</p>';

            }

        }

    endif;

}

function form( $i ) { ?>
    <section class="formularz" id="formularz">

        <div class="row">

            <div class="l12 col">

                <form method="POST" action="" id="form" class="form" data-id="1">

                    <div class="l12 center relative">

                        <img src="img/icon-form.svg" alt="" class="badge">
                        <strong id="zostaw-telefon">Zostaw nam kontakt</strong>
                        <p>a nasi doświadczeni <strong>doradcy</strong> przedstawią Ci najlepszą ofertę!</p>

                        <div class="l12">
                            <div class="field name">
                                <label>Imię i nazwisko <sup>*</sup></label>
                                <input type="text" name="name" value="<?= ( isset($_POST['name']) ) ? htmlspecialchars($_POST['name']) : ''; ?>" required tabindex="-1">
                                <span <?= ( $error['name'] == true ) ? 'class="show"' : '' ; ?>>Wypełnij poprawnie to pole</span>
                            </div>
                            <div class="field phone">
                                <label>Numer telefonu<sup>*</sup></label>
                                <input type="text" name="phone" value="<?= ( isset($_POST['phone']) ) ? htmlspecialchars($_POST['phone']) : ''; ?>" placeholder="___ ___ ___" required>
                                <span <?= ( $error['phone'] == true ) ? 'class="show"' : '' ; ?>>Wypełnij poprawnie to pole</span>
                            </div>
                            <div class="field email">
                                <label>Adres e-mail</label>
                                <input type="email" name="email" value="<?= ( isset($_POST['email']) ) ? htmlspecialchars($_POST['email']) : ''; ?>" placeholder="_____@_____.___">
                                <span <?= ( $error['email'] == true ) ? 'class="show"' : '' ; ?>>Wypełnij poprawnie to pole</span>
                            </div>
                        </div>

                        <div class="clearfix"></div>

                        <div class="field checkbox zgoda_handlowa">
                            <input type="checkbox" id="zgoda_handlowa<?= $i; ?>" name="zgoda_handlowa" required <?= ( isset($_POST['zgoda_handlowa']) == true ) ? 'checked="checked"' : ''; ?>>
                            <label for="zgoda_handlowa<?= $i; ?>">* Wyrażam zgodę na przetwarzanie moich danych osobowych do celów marketingowych, w tym do przesyłania informacji handlowych drogą elektroniczną przez AGROAS sp. z o.o. sp. k. Nowa Wieś Mała 27B 49-200 Grodków, zgodnie z art. 10 Ustawy z dnia 18 lipca 2002 r. o świadczeniu usług drogą elektroniczną (t.j. Dz.U. z 2019 r. poz. 123).</label>
                            <span <?= ( $error['zgoda_handlowa'] == true ) ? 'class="show"' : '' ; ?>>Zaznaczenie tego pola jest obowiązkowe</span>
                        </div>

                        <div class="field checkbox zgoda_handlowa2">
                            <input type="checkbox" id="zgoda_handlowa2_<?= $i; ?>" name="zgoda_handlowa2" required <?= ( isset($_POST['zgoda_handlowa2']) == true ) ? 'checked="checked"' : ''; ?>>
                            <label for="zgoda_handlowa2_<?= $i; ?>">* Wyrażam zgodę na przetwarzanie moich danych osobowych do celów marketingowych, w tym do używania telekomunikacyjnych urządzeń końcowych i automatycznych systemów wywołujących dla celów marketingu bezpośredniego przez AGROAS sp. z o.o. sp. k. Nowa Wieś Mała 27B 49-200 Grodków, zgodnie z art. 172 Ustawy z dnia 16 lipca 2004 r. Prawo telekomunikacyjne (t.j. Dz.U. z 2018 r. poz. 1954 ze zm.).</label>
                            <span <?= ( $error['zgoda_handlowa2'] == true ) ? 'class="show"' : '' ; ?>>Zaznaczenie tego pola jest obowiązkowe</span>
                        </div>

                        <div class="field">
                            <p class="small">* - pola obowiązkowe; ** – szczegóły w <a href="https://promocja.agroas.pl/agroas-regulamin.pdf" target="_blank">regulaminie</a>.</p>
                        </div>

                        <div class="l12 text-center">
                            <div class="button" disabled="true">
                                <input type="submit" value="Wyślij" class="form_submit" data-id="<?= $i; ?>">
                            </div>
                        </div>

                    </div>

                </form>

            </div>

        </div>

    </section>

    <?php
}

?>