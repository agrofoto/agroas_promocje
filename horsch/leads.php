<!DOCTYPE html>
<html>
    <head>
        <meta name="robots" content="noindex">
        <meta charset="utf-8">
        <title>Leady w&nbsp;kampanii AgroAs</title>
    </head>
    <body>
        <style>*{box-sizing:border-box}body{padding:2rem;text-align:center;font-family:Helvetica,Arial,sans-serif}form{max-width:320px;margin:0 auto}label{width:100%;display:block;margin-bottom:1rem}input[type=password]{width:100%;padding:.5rem;border:1px solid #ddd}input[type=submit]{width:100%;clear:both;margin:.5rem 0;padding:.5rem;border:0;background:#000;color:#fff;font-size:1rem}.message.negative{padding:.25rem;color:#fff;background:#c03}table{width:50%;float:left;border-collapse:collapse}table thead{font-weight:700;background:#f1f1f1}table tr:last-child td{font-weight:700}table tr td{padding:.25rem;border:1px solid #ddd;font-size:.85rem}</style>
<?php
$pass    = '@groAS2021!';
$is_post = ( $_SERVER[ 'REQUEST_METHOD' ] ?? '' ) === 'POST';
$_pass   = $_POST[ 'pass' ] ?? '';

if( $is_post && ( $pass === $_pass ) )
{
    require_once 'config.php';
?>
        <h1 style="text-align: left">Maszyny Horsch czekają na ciebie gotowe do odbioru!</h1>
<?php
    if( ( $mysqli = new mysqli( $config[ 'db_host' ] ?? '', $config[ 'db_user' ] ?? '', $config[ 'db_pass' ] ?? '', $config[ 'db_name' ] ?? '' ) ) && empty( $mysqli->connect_errno ) )
    {
        $mysqli->set_charset( 'utf8mb4' );
?>
        <table>
            <thead>
                <tr>
                    <td>Imię i&nbsp;nazwisko</td>
                    <td>Telefon</td>
                    <td>E-mail</td>
                    <td>Zgoda nr 1</td>
                    <td>Zgoda nr 2</td>
                    <td>Data</td>
                    <td>Adres IP</td>
                </tr>
            </thead>
            <tbody>
<?php
        if( $result = $mysqli->query( 'SELECT * FROM `lp_horsch` ORDER BY `lead_id` ASC' ) )
        {
            while( $lead = $result->fetch_assoc() )
            {
?>
                <tr>
                    <td><?= empty( $lead[ 'lead_name' ] ) ? '–' : $lead[ 'lead_name' ]; ?></td>
                    <td><?= empty( $lead[ 'lead_phone' ] ) ? '–' : $lead[ 'lead_phone' ]; ?></td>
                    <td><?= empty( $lead[ 'lead_email' ] ) ? '–' : $lead[ 'lead_email' ]; ?></td>
                    <td><?= ( ( $lead[ 'lead_consent_1' ] ?? '1' ) === '1' ) ? 'Tak' : 'Nie'; ?></td>
                    <td><?= ( ( $lead[ 'lead_consent_2' ] ?? '1' ) === '1' ) ? 'Tak' : 'Nie'; ?></td>
                    <td><?= empty( $lead[ 'lead_time' ] ) ? '–' : date( 'Y-m-d H:i:s', $lead[ 'lead_time' ] ); ?></td>
                    <td><?= empty( $lead[ 'lead_ip' ] ) ? '–' : $lead[ 'lead_ip' ]; ?></td>
                </tr>
<?php
            }
?>
                <tr>
                   <td colspan="7">Łącznie: <?= $result->num_rows ?? 0; ?></td>
                </tr>
<?php
        }
?>
            </tbody>
        </table>
<?php
    }
    else
    {
?>
        <p style="color: #f00;">Błąd połączenia z&nbsp;bazą danych</p>
<?php
    }

    die();
}
else
{
?>
        <form method="POST" action="">
<?php
    if( $is_post && ( $pass !== $_pass ) )
    {
?>
            <p class="message negative">Nieprawidłowe hasło</p>
<?php
    }
?>
            <label>Wpisz hasło dostępowe:</label>
            <input type="password" name="pass" required>
            <input type="submit" value="Zaloguj się">
        </form>
<?php
}
?>
    </body>
</html>