<?php require_once 'functions.php'; ?>
<!DOCTYPE html>
<html lang="pl">
    <head>
        <title>Maszyny Horsch czekają na Ciebie gotowe do odbioru! - AgroAs</title>
        <meta charset="utf-8">
        <meta name="robots" content="noindex">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
<?php
if( !$is_localhost )
{
?>
        <script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src='https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);})(window,document,'script','dataLayer','GTM-5CXJ7P3');</script>
<?php
}
?>
        <link rel="preconnect" href="https://fonts.googleapis.com">
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        <link rel="icon" href="<?= $base_url; ?>/img/favicon.png" type="image/png">
        <link href="https://fonts.googleapis.com/css2?family=Ubuntu:wght@400;500;700&display=swap" rel="stylesheet">
        <link href="<?= $base_url; ?>/css/bootstrap.min.css" rel="stylesheet">
        <link href="<?= $base_url; ?>/css/styles.min.css" rel="stylesheet">
    </head>
    <body data-body="<?= str_replace( '_', '-', $page ); ?>">
<?php
if( !$is_localhost )
{
?>
        <noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-5CXJ7P3" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<?php
}
?>
        <div id="wrapper">
            <div class="container">
                <header>
                    <div class="row no-gutters" data-row="logos">
                        <div class="col-6 col-lg-3" data-col="agroas">
                            <?php require_once 'img/agroas.svg'; ?>
                        </div>
                        <div class="col-6 col-lg-3" data-col="horsch">
                            <?php require_once 'img/horsch.svg'; ?>
                        </div>
                        <div class="col-12 col-lg-6" data-col="claim">
                            <p><strong>Maszyny Horsch</strong> czekają na Ciebie<br>gotowe do odbioru!</p>
                        </div>
                    </div>
                    <?php get_background(); ?>
                </header>
                <main><?php get_page(); ?></main>
                <footer>
                    <div class="row no-gutters" data-row="footer">
                        <div class="col-md-6">
                            <p>&copy; AgroAs 2021. Wszystkie prawa zastrzeżone.</p>
                        </div>
                        <div class="col-md-6">
                            <ul>
                                <li>
                                    <a href="https://agroas.pl/polityka-cookies" target="_blank">Polityka cookies</a>
                                </li>
                                <li>
                                    <a href="https://agroas.pl/polityka-prywatnosci" target="_blank">Polityka prywatności</a>
                                </li>
                                <li>
                                    <a href="https://agroas.pl/kontakt" target="_blank">Kontakt</a>
                                </li>
                            </ul>
                        </div>
                    </div>
<?php
if( ( $_COOKIE[ 'agroas_horsch_cookies_accepted' ] ?? '' ) !== '1' )
{
?>
                    <div class="row no-gutters" data-row="cookies">
                        <div class="col">
                            <p>Serwis korzysta z&nbsp;plików cookies w&nbsp;zakresie opisanym w&nbsp;<a href="https://agroas.pl/polityka-cookies" target="_blank">Polityce cookies</a>. Korzystając z&nbsp;serwisu wyrażasz zgodę na używanie cookies zgodnie z&nbsp;ustawieniami przeglądarki.</p>
                            <button class="button button-contrast">Rozumiem <?php require 'img/arrow.svg'; ?></button>
                            <script>
                                document.querySelector( '[data-row="cookies"] .button' ).addEventListener( 'click', function( event ) {
                                    event.preventDefault();
                                    document.cookie = 'agroas_horsch_cookies_accepted=1;max-age=2592000;path=/';
                                    event.target.parentElement.parentElement.parentElement.removeChild( event.target.parentElement.parentElement );
                                } );
                            </script>
                        </div>
                    </div>
<?php
}
?>
                </footer>
            </div>
        </div>
        <script src="<?= $base_url; ?>/js/scripts.min.js"></script>
    </body>
</html>