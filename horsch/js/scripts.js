window.addEventListener( 'load', function() {
    function handleBackgroundMargins()
    {
        const bgNode     = document.querySelector( '[data-row="bg-wrapper"] img' );
        const claimNode  = document.querySelector( '[data-col="claim"] p' );
        const isMobile   = ( window.innerWidth < 992 );
        var marginTop    = 0;

        if( bgNode !== null )
        {
            const marginBottom = 0.409 * bgNode.clientHeight;
            marginTop          = 0.1125 * bgNode.clientHeight;

            bgNode.style.marginTop    = isMobile ? '' : ( '-' + marginTop + 'px' );
            bgNode.style.marginBottom = isMobile ? '' : ( '-' + marginBottom + 'px' );   
        }

        if( claimNode !== null )
        {
            const agroasNode = document.querySelector( '[data-col="agroas"] svg' );
            const horschNode = document.querySelector( '[data-col="horsch"] svg' );
            const height     = claimNode.clientHeight - marginTop;

            if( agroasNode !== null )
            {
                agroasNode.style.marginTop = isMobile ? '' : ( ( height - agroasNode.clientHeight ) / 2 + 'px' );
            }

            if( horschNode !== null )
            {
                horschNode.style.marginTop = isMobile ? '' : ( ( height - horschNode.clientHeight ) / 2 + 'px' );
            }
        }
    }

    var resizeTimeout;

    handleBackgroundMargins();
    window.addEventListener( 'resize', function() {
        clearTimeout( resizeTimeout );
        resizeTimeout = window.setTimeout( handleBackgroundMargins, 1000 );
    } );
} );