<?php
function get_background()
{
    global $page, $base_url;

    if( $page !== 'thank_you')
    {
?>
<div class="row no-gutters" data-row="bg-wrapper">
    <div class="col">
        <picture>
            <source srcset="<?= $base_url; ?>/img/maszyny-horsch.jpg" media="(max-width:767px)">
            <img src="<?= $base_url; ?>/img/maszyny-horsch-1.jpg" alt="maszyny Horsch dostępne od ręki">
        </picture>
    </div>
</div>
<?php
    }
}

function get_page()
{
    global $page, $base_url;

    if( $page === 'thank_you' )
    {
?>
<div class="row no-gutters" data-row="thank-you">
    <div class="col">
        <h1><span>Dziękujemy</span> za przesłanie formularza!</h1>
        <p>Już wkrótce skontaktujemy się z&nbsp;Tobą i&nbsp;pomożemy Ci wybrać odpowiednią <strong>maszynę Horsch</strong>.</p>
        <p>Do usłyszenia!<br>Zespół <span>AgroAs</span></p>
    </div>
</div>
<?php
    }
    else
    {
        global $validation_errors;
?>
<div class="row no-gutters" data-row="form-wrapper">
    <div class="col-lg-6" data-col="intro">
        <h1>Jesienna oferta <span>szybki odbiór bez&nbsp;straty czasu</span></h1>
    </div>
    <div class="col-lg-6" data-col="form" id="formularz">
        <h2>Zostaw nam kontakt do siebie.</h2>
        <p>Oddzwonimy i&nbsp;<strong>pomożemy Ci wybrać</strong> odpowiednią maszynę.</p>
        <form method="post" action="<?= $base_url; ?>/">
<?php
        if( !empty( $validation_errors ) )
        {
?>
            <div class="alert alert-danger" role="alert">
                <p><?= implode( '<br>', $validation_errors ); ?></p>
            </div>
<?php
        }
?>
            <div class="form-row">
                <div class="form-group col">
                    <input type="text" class="form-control" placeholder="Imię i nazwisko*" name="name" value="<?= $_POST[ 'name' ] ?? ''; ?>" required>
                </div>
            </div>
            <div class="form-row">
                <div class="form-group col-md-5">
                    <input type="tel" class="form-control" placeholder="Numer telefonu*" maxlength="9" name="phone" value="<?= $_POST[ 'phone' ] ?? ''; ?>" required>
                </div>
                <div class="form-group col-md-7">
                    <input type="email" class="form-control" placeholder="Adres e-mail" name="email" value="<?= $_POST[ 'email' ] ?? ''; ?>">
                </div>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="1" name="consent_1" id="consent-1" <?php if( ( $_POST[ 'consent_1' ] ?? '-1' ) === '1' ):?>checked<?php endif;?>>
                <label class="form-check-label" for="consent-1">Wyrażam zgodę na przetwarzanie moich danych osobowych do celów marketingowych, w&nbsp;tym do przesyłania informacji handlowych drogą elektroniczną przez AGROAS sp. z&nbsp;o.o. sp. k. Nowa Wieś Mała 27B 49-200 Grodków, zgodnie z&nbsp;art. 10 Ustawy z&nbsp;dnia 18 lipca 2002 r. o&nbsp;świadczeniu usług drogą elektroniczną (t.j. Dz.U. z&nbsp;2019 r. poz. 123).</label>
            </div>
            <div class="form-check">
                <input class="form-check-input" type="checkbox" value="1" name="consent_2" id="consent-2" <?php if( ( $_POST[ 'consent_2' ] ?? '-1' ) === '1' ):?>checked<?php endif;?> required>
                <label class="form-check-label" for="consent-2">* Wyrażam zgodę na przetwarzanie moich danych osobowych do celów marketingowych, w&nbsp;tym do używania telekomunikacyjnych urządzeń końcowych i&nbsp;automatycznych systemów wywołujących dla celów marketingu bezpośredniego przez AGROAS sp. z&nbsp;o.o. sp. k. Nowa Wieś Mała 27B 49-200 Grodków, zgodnie z&nbsp;art. 172 Ustawy z&nbsp;dnia 16 lipca 2004 r. Prawo telekomunikacyjne (t.j. Dz.U. z&nbsp;2018 r. poz. 1954 ze zm.).</label>
            </div>
            <div class="form-check">
                <label class="form-check-label">* pola obowiązkowe</label>
            </div>
            <button class="button" type="submit">Wyślij <?php require_once 'img/arrow.svg'; ?></button>
        </form>
    </div>
</div>
<div class="row no-gutters" data-row="contact-us">
    <div class="col-12">
        <p>Zadzwoń do nas i&nbsp;spytaj naszych ekspertów o&nbsp;szczegóły:<br><a href="tel:+48795107313">+48 795 107 313</a></p>
    </div>
</div>
<div class="row no-gutters">
    <div class="col-md-6 col-lg-4" data-col="model">
        <picture>
            <source srcset="<?= $base_url; ?>/img/horsch-terrano-fx.jpg" media="(max-width:767px)">
            <img src="<?= $base_url; ?>/img/horsch-terrano-fx-1.jpg" alt="Horsch Terrano FX">
        </picture>
        <h3>Kultywator <span>Horsch Terrano FX</span></h3>
        <p>Maszyna o&nbsp;najwyższych standardach. Kultywator uniwersalny, który może być wyposażony w&nbsp;różne rodzaje narzędzi roboczych.</p>
        <p>Pozwoli Ci sprawnie i&nbsp;szybko wykonać zaplanowane zadania. Jest kompaktowy i&nbsp;może być używany do płytkiej uprawy gleby na całej powierzchni lub do intensywnego mieszania słomy i&nbsp;resztek pożniwnych na głębokościach od 5&nbsp;do 30 cm. Masywna, 3-belkowa konstrukcja o&nbsp;dużym prześwicie zapewnia równomierne wymieszanie słomy z&nbsp;glebą, nawet w&nbsp;trudnych warunkach.</p>
    </div>
    <div class="col-md-6 col-lg-4" data-col="model">
        <picture>
            <source srcset="<?= $base_url; ?>/img/horsch-terrano-gx.jpg" media="(max-width:767px)">
            <img src="<?= $base_url; ?>/img/horsch-terrano-gx-1.jpg" alt="Horsch Terrano GX">
        </picture>
        <h3>Agregat uprawowy <span>Horsch Terrano GX</span></h3>
        <p>To wszechstronne i&nbsp;precyzyjne narzędzie, które umożliwi Ci perfekcyjną – zarówno płytką, jak i&nbsp;głęboką – uprawę, sięgającą 25&nbsp;cm.</p>
        <p>Terrano GX jest dostępny jako uniwersalny kultywator 3- i&nbsp;4-belkowy, a&nbsp;jego szerokości robocze wynoszą od 4&nbsp;do 6&nbsp;metrów. Posiada zęby TerraGrip trzeciej generacji i&nbsp;hydrauliczne wzmocnienie siły uciągu, które podczas pracy stale dociska tylną oś ciągnika ciężarem 1200&nbsp;kg.</p>
    </div>
    <div class="col-md-6 col-lg-4" data-col="model">
        <picture>
            <source srcset="<?= $base_url; ?>/img/horsch-joker-rt.jpg" media="(max-width:767px)">
            <img src="<?= $base_url; ?>/img/horsch-joker-rt-1.jpg" alt="Horsch Joker RT">
        </picture>
        <h3>Agregat talerzowy <span>Horsch Joker RT</span></h3>
        <p>Łatwy w&nbsp;obsłudze, czysty, o&nbsp;dużej wydajności i&nbsp;jakości pracy. Agregat talerzowy Horsch dostępny jest w&nbsp;szerokościach roboczych 5, 6&nbsp;i&nbsp;7,25&nbsp;m.</p>
        <p>Nadaje się do płytkiej uprawy ścierniska w&nbsp;celu stymulacji kiełkowania samosiewów, przerywania kapilarności, niszczenia resztek pożniwnych i&nbsp;optymalnego przygotowania pola do siewu. Co więcej, posiada również wszechstronne wały do wszystkich rodzajów gleby, a&nbsp;podwozie transportowe maszyny zapewnia jej pewny i&nbsp;szybki transport po drogach publicznych.</p>
    </div>
    <div class="col-md-6 col-lg-4" data-col="model">
        <picture>
            <source srcset="<?= $base_url; ?>/img/horsch-joker-hd.jpg" media="(max-width:767px)">
            <img src="<?= $base_url; ?>/img/horsch-joker-hd-1.jpg" alt="Horsch Joker HD">
        </picture>
        <h3>Brona talerzowa <span>Horsch Joker HD</span></h3>
        <p>To maszyna o&nbsp;dużej wydajności przy relatywnie niewielkim zapotrzebowaniu na moc.</p>
        <p>Zapewnia dużą zawartość rozdrobnionej gleby w&nbsp;strefie wschodów oraz skuteczne pasowe zagęszczanie gleby dzięki podwójnemu wałowi RollPack o&nbsp;dużej nośności i&nbsp;dużej zdolności wyrównywania. Umożliwia uprawę gleby do głębokości 15 cm, poprzez bardzo dobre cięcie i&nbsp;mieszanie dużych ilości resztek pożniwnych, np. słomy z&nbsp;kukurydzy na ziarno.</p>
    </div>
    <div class="col-md-6 col-lg-4" data-col="model">
        <picture>
            <source srcset="<?= $base_url; ?>/img/horsch-versa-kr.jpg" media="(max-width:767px)">
            <img src="<?= $base_url; ?>/img/horsch-versa-kr-1.jpg" alt="Horsch Versa KR">
        </picture>
        <h3>Kombinacja uprawowo-siewna <span>Horsch Versa KR</span></h3>
        <p>To zawieszany mechaniczny siewnik z&nbsp;broną wirnikową do zboża i&nbsp;rzepaku.</p>
        <p>Zastosowano w&nbsp;nim innowacyjne rozwiązania techniczne w&nbsp;zakresie dozowania i&nbsp;włączania ścieżek technologicznych. Zastosowane podłączenie dozowników umożliwia indywidualną i&nbsp;beznarzędziową zmianę ścieżek technologicznych. A&nbsp;nowa koncepcja obsługi została zoptymalizowana pod kątem terminali dotykowych, dzięki czemu umożliwia indywidualne ustawienie opcji wyświetlania. Czysto mechaniczne systemy dozowania i&nbsp;ich zalety są jednak również bardzo popularne wśród naszych klientów w&nbsp;segmencie maszyn 3m.</p>
    </div>
    <div class="col-md-6 col-lg-4" data-col="model">
        <picture>
            <source srcset="<?= $base_url; ?>/img/horsch-express-td.jpg" media="(max-width:767px)">
            <img src="<?= $base_url; ?>/img/horsch-express-td-1.jpg" alt="Horsch Express TD">
        </picture>
        <h3>Agregat uprawowo-siewny <span>Horsch Express TD</span></h3>
        <p>Jest to zawieszany pneumatyczny siewnik z&nbsp;broną wirnikową (Mini Pronto).</p>
        <p>Łączy zalety siewu zestawem Pronto na 3-punktowym układzie zawieszenia ciągnika z&nbsp;zaletami zwrotnego siewnika zawieszanego na TUZ-ie. Dzięki dużej przepustowości talerze o&nbsp;wymiarze 460 mm są odporne na zapychanie nawet przy dużej ilości materiału organicznego. Głębokość roboczą redlic DiscSystems można wygodnie ustawiać hydraulicznie z&nbsp;ciągnika.</p>
    </div>
    <div class="col-md-6 col-lg-4" data-col="model">
        <picture>
            <source srcset="<?= $base_url; ?>/img/horsch-pronto-dc.jpg" media="(max-width:767px)">
            <img src="<?= $base_url; ?>/img/horsch-pronto-dc-1.jpg" alt="Horsch Pronto DC">
        </picture>
        <h3>Siewnik <span>Horsch Pronto DC</span></h3>
        <p>Uniwersalny ciągany zestaw uprawowo-siewny, gwarantuje idealny wysiew po różnych pracach przygotowawczych, który jest świetnym narzędziem do szerokiej gamy nasion.</p>
        <p>Oprócz rzepaku, pszenicy, jęczmienia, można nim również wysiewać np. trawę. Gwarantuje też idealny wysiew po różnych pracach przygotowawczych, w&nbsp;tym: wysiew w&nbsp;mulcz, po pługu lub po intensywnym mieszaniu gleby. To najpopularniejszy siewnik w&nbsp;Europie do uprawy uproszczonej.</p>
    </div>
    <div class="col-md-6 col-lg-4" data-col="model">
        <picture>
            <source srcset="<?= $base_url; ?>/img/horsch-focus-td.jpg" media="(max-width:767px)">
            <img src="<?= $base_url; ?>/img/horsch-focus-td-1.jpg" alt="Horsch Focus TD">
        </picture>
        <h3>Agregat do uprawy pasowej technologii Strip-Till <span>Horsch Focus TD</span></h3>
        <p>Ciągany zestaw uprawowo-siewny, który podczas jednego przejazdu doglebowo odkłada nawóz (na jednej bądź dwóch głębokościach) oraz jednocześnie zagęszcza i&nbsp;wysiewa nasiona zbóż lub rzepaku.</p>
        <p>Co więcej, w&nbsp;zależności od potrzeb talerze wklęsłe pracują między rzędami siewnymi, tworząc redliny lub wyrównując powierzchnię.</p>
    </div>
    <div class="col-lg-4" data-col="call">
        <?php require_once 'img/call.svg'; ?>
        <p>Nie jesteś pewien, <strong>jaki sprzęt</strong> jest dla Ciebie odpowiedni?</p>
        <p>Zostaw nam swój numer telefonu.</p>
        <p><strong>Zadzwonimy do Ciebie i&nbsp;ustalimy,</strong> jaka maszyna przyda Ci się najbardziej.</p>
        <p>Pamiętaj! Maszyny są <strong>dostępne od ręki</strong> – nie będziesz musiał na nie czekać tygodniami.<br><a class="button button-contrast" href="#formularz">Wyślij <?php require 'img/arrow.svg'; ?></a></p>
    </div>
</div>
<div class="row no-gutters justify-content-center" data-row="story-offer">
    <div class="col-md-12">
        <h2>Kim jesteśmy?</h2>
    </div>
    <div class="col-md-6" data-col="story">
        <p>AGROAS to lider rynku działający w&nbsp;branży kompleksowego zaopatrzenia rolnictwa i&nbsp;obrotu płodami rolnymi. Działamy od 1992 roku i&nbsp;jesteśmy jedną z&nbsp;najszybciej rozwijających się firm w&nbsp;naszym regionie.</p>
        <p>Obsługujemy ponad 5000 Klientów w&nbsp;Polsce południowo-zachodniej – zarówno rolników z&nbsp;małym i&nbsp;średnim areałem, jak i&nbsp;gospodarstwa wielkoobszarowe, spółdzielnie rolnicze i&nbsp;spółki działające w&nbsp;sektorze rolnym.</p>
        <p>Zadzwoń do nas i&nbsp;spytaj naszych ekspertów:<br><a href="tel:+48795107313">+48 795 107 313</a></p>
    </div>
    <div class="col-md-6" data-col="offer">
        <p>Potrzebny Ci kultywator do ciągnika? Uprawowy czy ścierniskowy?</p>
        <p>W swojej ofercie mamy kultywatory i&nbsp;inne maszyny rolnicze Horsch.</p>
    </div>
</div>
<?php
    }
}

function send_emails( $lead )
{
    global $base_url, $config;

    if( !isset( $lead[ 'lead_name' ] ) || !isset( $lead[ 'lead_phone' ] ) || !isset( $lead[ 'lead_email' ] ) || !isset( $lead[ 'lead_consent_1' ] ) || !isset( $lead[ 'lead_consent_2' ] ) || !isset( $lead[ 'lead_time' ] ) || !isset( $lead[ 'lead_ip' ] ) )
    {
        return false;
    }

    $subject_1    = 'AgroAs - maszyny Horsch gotowe do odbioru - potwierdzenie wypełnienia formularza';
    $subject_2    = substr( $subject_1, 0, strrpos( $subject_1, '-' ) + 2 ) . 'nowe zgłoszenie';
    $notify_name  = $config[ 'notify_name' ] ?? 'dev Adagri';
    $notify_email = $config[ 'notify_email' ] ?? 'dev@dev.adagri.com';
    $status_1     = empty( $lead[ 'lead_email' ] ) ?: false;

    if( !$status_1 )
    {
        ob_start();
?>
<!DOCTYPE html>
<html lang="pl">
    <head>
        <title><?= $subject_1; ?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
    </head>
    <body>
        <img src="<?= $base_url; ?>/img/agroas.png" alt="AgroAs">
        <p style="font-weight: 700">Dziękujemy za przesłanie formularza!<p>
        <p>Już wkrótce skontaktujemy się z&nbsp;Tobą i&nbsp;pomożemy Ci wybrać odpowiednią <strong>maszynę Horsch</strong>.</p>
        <p>Do usłyszenia!<br>Zespół AgroAs</p>
    </body>
</html>
<?php
        $status_1 = mail( "{$lead[ 'lead_name' ]} <{$lead[ 'lead_email' ]}>", $subject_1, ob_get_clean(), "MIME-Version: 1.0\r\nContent-type: text/html; charset=UTF-8\r\nFrom: AgroAs <automat@agroas.pl>" );
    }

    ob_start();
?>
<!DOCTYPE html>
<html lang="pl">
    <head>
        <title><?= $subject_2; ?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width,initial-scale=1.0">
    </head>
    <body>
        <img src="<?= $base_url; ?>/img/agroas.png" alt="AgroAs">
        <p style="font-weight: 700">NOWA WIADOMOŚĆ NA LP „MASZYNY HORSCH CZEKAJĄ NA CIEBIE GOTOWE DO ODBIORU!”.</p>
        <p>Wysłane zostały następujące dane:<p>
        <p>Imię i&nbsp;nazwisko: <strong><?= $lead[ 'lead_name' ]; ?></strong><br>E-mail: <strong><?= empty( $lead[ 'lead_email' ] ) ? '–' : $lead[ 'lead_email' ]; ?></strong><br>Telefon: <strong><?= $lead[ 'lead_phone' ]; ?></strong><br>Zgoda nr 1: <strong><?= ( $lead[ 'lead_consent_1' ] === '1' ? 'Tak' : 'Nie' ); ?></strong><br>Zgoda nr 2: <strong><?= ( $lead[ 'lead_consent_2' ] === '1' ? 'Tak' : 'Nie' ); ?></strong></p>
        <p>Do usłyszenia!<br>Zespół AgroAs</p>
    </body>
</html>
<?php
    $status_2 = mail( "{$notify_name} <{$notify_email}>", $subject_2, ob_get_clean(), "MIME-Version: 1.0\r\nContent-type: text/html; charset=UTF-8\r\nFrom: AgroAs <automat@agroas.pl>" );

    return ( !empty( $status_1 ) && !empty( $status_2 ) );
}

function insert_lead( $lead )
{
    global $config;

    if( isset( $lead[ 'lead_name' ] ) && isset( $lead[ 'lead_phone' ] ) && isset( $lead[ 'lead_email' ] ) && isset( $lead[ 'lead_consent_1' ] ) && isset( $lead[ 'lead_consent_2' ] ) && isset( $lead[ 'lead_time' ] ) && isset( $lead[ 'lead_ip' ] ) && ( $mysqli = new mysqli( $config[ 'db_host' ] ?? '', $config[ 'db_user' ] ?? '', $config[ 'db_pass' ] ?? '', $config[ 'db_name' ] ?? '' ) ) && empty( $mysqli->connect_errno ) && ( $stmt = $mysqli->prepare( 'INSERT INTO `lp_horsch` (`lead_name`, `lead_phone`, `lead_email`, `lead_consent_1`, `lead_consent_2`, `lead_time`, `lead_ip`) VALUES (?, ?, ?, ?, ?, ?, ?)' ) ) )
    {
        $mysqli->set_charset( 'utf8mb4' );
        $success = ( $stmt->bind_param( 'sssssss', $lead[ 'lead_name' ], $lead[ 'lead_phone' ], $lead[ 'lead_email' ], $lead[ 'lead_consent_1' ], $lead[ 'lead_consent_2' ], $lead[ 'lead_time' ], $lead[ 'lead_ip' ] ) && $stmt->execute() );
        $stmt->close();
    }

    return !empty( $success );
}

function get_validation_errors()
{
    if( !empty( $_POST[ 'email' ] ) && !filter_var( $_POST[ 'email' ], FILTER_VALIDATE_EMAIL ) )
    {
        $errors[] = 'Podaj prawidłowy adres e-mail.';
    }

    if( empty( $_POST[ 'name' ] ) )
    {
        $errors[] = 'Podaj imię i&nbsp;nazwisko.';
    }

    if( !preg_match( '/^\d{9}$/', $_POST[ 'phone' ] ?? '' ) )
    {
        $errors[] = 'Podaj prawidłowy numer telefonu (9 cyfr obok siebie).';
    }

    if( ( $_POST[ 'consent_2' ] ?? '-1' ) !== '1' )
    {
        $errors[] = 'Musisz wyrazić zgodę na przetwarzanie danych osobowych.';
    }

    return $errors ?? array();
}

require_once 'config.php';

$whitelist    = array( 'localhost', '127.0.0.1', '::1', 'agroas.lo' );
$is_localhost = ( in_array( $_SERVER[ 'REMOTE_ADDR' ] ?? '', $whitelist ) || in_array( $_SERVER[ 'HTTP_HOST' ] ?? '', $whitelist ) );
$base_url     = $config[ 'base_url' ] ?? '';
$page         = $_GET[ 'page' ] ?? 'index';

if( session_status() === PHP_SESSION_NONE )
{
    session_start();
}

if( $_SERVER[ 'REQUEST_METHOD' ] === 'POST' )
{
    if( empty( $validation_errors = get_validation_errors() ) )
    {
        $lead = array(
            'lead_name'      => htmlspecialchars( $_POST[ 'name' ] ),
            'lead_phone'     => $_POST[ 'phone' ],
            'lead_email'     => $_POST[ 'email' ] ?? '',
            'lead_consent_1' => empty( $_POST[ 'consent_1' ] ) ? '0' : '1',
            'lead_consent_2' => $_POST[ 'consent_2' ],
            'lead_time'      => time(),
            'lead_ip'        => $_SERVER[ 'REMOTE_ADDR' ] ?? ''
        );

        if( insert_lead( $lead ) && send_emails( $lead ) )
        {
            $_SESSION[ 'show_thank_you_page' ] = true;
            header( "Location: {$base_url}/dziekujemy/" );
            exit;
        }
        else
        {
            $validation_errors = array( 'Twoje zgłoszenie nie zostało wysłane. Spróbuj ponownie!' );
        }
    }
}
else
{
    if( $page === 'thank_you' )
    {
        if( empty( $_SESSION[ 'show_thank_you_page' ] ) )
        {
            header( "Location: {$base_url}" );
            exit;
        }

        unset( $_SESSION[ 'show_thank_you_page' ] );
    }
}
?>